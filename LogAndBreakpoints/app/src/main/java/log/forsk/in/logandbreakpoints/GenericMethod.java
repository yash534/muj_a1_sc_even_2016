package log.forsk.in.logandbreakpoints;

/**
 * Created by Saurabh on 2/8/2016.
 */
public class GenericMethod {

    // generic method printArray
    public static < E > void printArray( E[] inputArray )
    {
        // Display array elements
        for ( E element : inputArray ){
            System.out.printf( "%s ", element );
        }
        System.out.println();
    }


}
