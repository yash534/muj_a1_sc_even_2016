package log.forsk.in.logandbreakpoints;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Saurabh on 2/8/2016.
 */

//New comment added on 2/10/2016
public class MyModel {

    public static final String FIRSTNAME = "firstName";
    public static final String LASTNAME = "lastName";

    private List<Person> persons = new ArrayList<Person>();

    public class Person {

        private String firstName;

        private String lastName;

        public Person(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public String getFirstName() {

            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;

        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        @Override
        public String toString() {
            return "First Name : "+firstName +"\n Last Name : "+lastName;
        }
    }

    public List<Person> getPersons() {
        return persons;
    }

    public MyModel() {
        // just for testing we hard-code the persons here:
        persons.add(new Person("Yogendra", "Sharma"));
        persons.add(new Person("Rohit", "Mishra"));
    }


}
