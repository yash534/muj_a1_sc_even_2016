package in.forsk.classwork1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class ScreenA extends AppCompatActivity implements View.OnClickListener {
    //TAG for logging
    private final static String TAG = ScreenA.class.getSimpleName();

    //Key for random string been stored in bundle(intent)
    public final static String RANDOM_STRING = "random_from_screen_a";

    //Reference Variable for Xml Views
    private Button nextBtn;
    private TextView randomTv;

    //Reference Variable for random class
    private Random random;

    //Class Variable to store the random int
    private int random_int = -1;

    //Class variable to store the random int string
    private String random_string = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Do not delete or modify these two lines

        //https://docs.oracle.com/javase/tutorial/java/IandI/super.html
        super.onCreate(savedInstanceState);

        //The Activity class takes care of creating a window for you in which you can place your UI with setContentView(View)
        setContentView(R.layout.activity_screen1);

        //Start writing your code from here.

        //init random variable
        //always check for null before creating object.
        if (random == null) {
            random = new Random();
        } else {
            Log.d(TAG, "Random Object already in memory !!");
        }

        //point the reference variable to the repective object of views in XML
        nextBtn = (Button) findViewById(R.id.nextBtn);
        randomTv = (TextView) findViewById(R.id.randomTv);

        //Setting on Click listener
        nextBtn.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        //Generating random int
        //best practice is to always put your code in try & catch block.
        try {
            random_int = random.nextInt();
        } catch (Exception e) {

            Log.e(TAG, "Random Variable is null");
            e.printStackTrace();
        }

        //Convert int to string
        random_string = String.valueOf(random_int);

        //Setting random text to textVie
        randomTv.setText(random_string);

//        randomTv.setText(String.valueOf(random.nextInt()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextBtn:

                //Create a intent to launch Screen B
                Intent launchScreenIntent = new Intent(ScreenA.this, ScreenB.class);

                //Put the random variable in bundle (intent)
                launchScreenIntent.putExtra(ScreenA.RANDOM_STRING,random_string);

                //Starting the activity by consuming the newly created intent.
                startActivity(launchScreenIntent);

//                startActivity(new Intent(ScreenA.this,ScreenB.class));
                break;
        }
    }
}
